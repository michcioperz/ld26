package pl.ijestfajnie.npc;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;

public class ExplodeControl extends AbstractControl implements PhysicsCollisionListener {

    Node rootNode;
    AssetManager assetManager;
    public float speed;
    private LD26 app;

    public ExplodeControl(LD26 app) {
        rootNode = app.getRootNode();
        assetManager = app.getAssetManager();
        this.app = app;
        app.getStateManager().getState(BulletAppState.class).getPhysicsSpace().addCollisionListener(this);
    }

    public void explode() {
        app.addScore(spatial.getControl(RigidBodyControl.class).getLinearVelocity().length());
    }

    @Override
    protected void controlUpdate(float tpf) {
        speed = spatial.getControl(RigidBodyControl.class).getLinearVelocity().length();
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {

    }

    public Control cloneForSpatial(Spatial spatial) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    float minSpeedToXpl = 25;

    public void collision(PhysicsCollisionEvent pce) {
        if ((pce.getNodeA().getControl(RigidBodyControl.class).getLinearVelocity().length() > 25 || pce.getNodeB().getControl(RigidBodyControl.class).getLinearVelocity().length() > 25)) {
            explode();
        }
    }

}