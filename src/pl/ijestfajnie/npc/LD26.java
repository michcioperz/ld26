package pl.ijestfajnie.npc;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.audio.Environment;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.HeightfieldCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.input.ChaseCamera;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.HDRRenderer;
import com.jme3.post.filters.ColorOverlayFilter;
import com.jme3.scene.Geometry;
import com.jme3.scene.debug.Arrow;
import com.jme3.scene.shape.PQTorus;
import com.jme3.shadow.BasicShadowRenderer;
import com.jme3.system.AppSettings;
import com.jme3.system.NanoTimer;
import com.jme3.terrain.geomipmap.TerrainGrid;
import com.jme3.terrain.geomipmap.TerrainGridListener;
import com.jme3.terrain.geomipmap.TerrainGridLodControl;
import com.jme3.terrain.geomipmap.TerrainGridTileLoader;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LD26 extends SimpleApplication implements ScreenController, AnalogListener {

    TerrainGrid terrain;
    Nifty nif;
    ChaseCamera chaseCam;
    Geometry cube;
    Geometry arrow;
    float puzzleR;

    public static void main(String[] args) {
        AppSettings settings = new AppSettings(true);
        settings.setSettingsDialogImage("sam.png");
        settings.setTitle("Midnight Inertia");
        LD26 app = new LD26();
        app.setSettings(settings);
        app.setShowSettings(true);
        app.setDisplayStatView(false);
        app.start();
    }
    private FilterPostProcessor fpp;
    private ColorOverlayFilter throwback;

    @Override
    public void simpleInitApp() {
        HDRRenderer hdr = new HDRRenderer(assetManager, renderer);
        hdr.setMaxIterations(40);
        hdr.setSamples(settings.getSamples());
        hdr.setWhiteLevel(3);
        hdr.setExposure(0.72f);
        hdr.setThrottle(1);
        viewPort.addProcessor(hdr);
        score = 0;
        puzzleR = 0;
        initSound();
        initInput();
        initGui();
    }

    @Override
    public void stop() {
        super.stop();
    }

    float score;

    @Override
    public void simpleUpdate(float tpf) {
        if (cube != null /*&& ghostie != null*/ && cube.getControl(ExplodeControl.class) != null && !nif.getCurrentScreen().getScreenId().equals("start") && puzzleR <= 30000f) {
            if (arrow != null) {
                rootNode.detachChild(arrow);
            }
            Arrow arry = new Arrow(cube.getControl(RigidBodyControl.class).getLinearVelocity());
            arrow = new Geometry("axis", arry);
            arrow.setMaterial(new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md"));
            arrow.getMaterial().setColor("Color", ColorRGBA.randomColor());
            arrow.setLocalTranslation(cube.getWorldTranslation());
            rootNode.attachChild(arrow);

            puzzleR += tpf * 1000 * speed;
            int noah = (int) (30000f - puzzleR) / 1000 * 42;
            if (nif.getCurrentScreen().getScreenId().equals("hud")) {
                nif.getCurrentScreen().findElementByName("overlay").findElementByName("score").findElementByName("Tim").getRenderer(TextRenderer.class).setText(Integer.toHexString(noah));
            }
        }
        if (puzzleR > 30000f && !nif.getCurrentScreen().getScreenId().equals("gamovr")) {
            inputManager.clearMappings();
            nif.getScreen("gamovr").findElementByName("ovrlay").findElementByName("ovrpan").findElementByName("itsover").getRenderer(TextRenderer.class).setText(Integer.toBinaryString(06) + " x " + Integer.toBinaryString(011) + " = " + Integer.toHexString((int) score));
            nif.gotoScreen("gamovr");
        }
    }

    public float addScore(float sc) {
        this.score += sc;

        return this.score;
    }

    private void initTerrain() {
        Material mat_ter = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        mat_ter.setTexture("DiffuseMap", assetManager.loadTexture("placeholder.png"));
        mat_ter.setTexture("NormalMap", assetManager.loadTexture("placeholder_1.png"));
        terrain = new TerrainGrid("terrain", 129, 129, new TerrainGridTileLoader() {

            int patchSize = 129;
            int quadSize = 129;

            public TerrainQuad getTerrainQuadAt(Vector3f location) {
                return new TerrainQuad("Terrain", patchSize, quadSize, new float[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0});
            }

            public void setPatchSize(int patchSize) {
                this.patchSize = patchSize;
            }

            public void setQuadSize(int quadSize) {
                this.quadSize = quadSize;
            }

            public void write(JmeExporter ex) throws IOException {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void read(JmeImporter im) throws IOException {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });
        terrain.setMaterial(mat_ter);
        rootNode.attachChild(terrain);
        this.terrain.setLocalTranslation(0, 0, 0);
        this.terrain.scale(10);
        TerrainLodControl control = new TerrainGridLodControl(this.terrain, cam);
        control.setEnabled(true);
        this.terrain.addControl(control);
        terrain.addListener(new TerrainGridListener() {
            public void gridMoved(Vector3f newCenter) {
            }

            public void tileAttached(Vector3f cell, TerrainQuad quad) {
                while(quad.getControl(RigidBodyControl.class)!=null){
                    quad.removeControl(RigidBodyControl.class);
                }
                quad.addControl(new RigidBodyControl(new HeightfieldCollisionShape(quad.getHeightMap(), terrain.getLocalScale()), 0));
                getPhysSpace().getPhysicsSpace().add(quad);
            }

            public void tileDetached(Vector3f cell, TerrainQuad quad) {
                getPhysSpace().getPhysicsSpace().remove(quad);
                while(quad.getControl(RigidBodyControl.class)!=null){
                    quad.removeControl(RigidBodyControl.class);
                }
            }
        });
    }

    private void initGui() {
        NiftyJmeDisplay ndis = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
        nif = ndis.getNifty();
        Logger.getLogger("de.lessvoid.nifty").setLevel(Level.WARNING);
        Logger.getLogger("NiftyInputEventHandlingLog").setLevel(Level.WARNING);
        nif.fromXml("pl/ijestfajnie/npc/LD26.xml", "start", this);
        guiViewPort.addProcessor(ndis);
        scr = this;
    }

    private void initPhysSpace() {
        BulletAppState bullet = new BulletAppState();
        this.getStateManager().attach(bullet);
        //bullet.getPhysicsSpace().enableDebug(assetManager);
        bullet.getPhysicsSpace().setGravity(new Vector3f(0, 0.5f, 0));
    }

    public BulletAppState getPhysSpace() {
        return getStateManager().getState(BulletAppState.class);
    }

    public void startGame() {
        initPhysSpace();
        initHero();
        initCam();
        initTerrain();
        chaseCam.setDragToRotate(false);
        nif.fromXml("pl/ijestfajnie/npc/LD26.xml", "hud", this);
    }

    public void bind(Nifty nifty, Screen screen) {    }

    public void onStartScreen() {    }

    public void onEndScreen() {    }

    private void initCam() {
        flyCam.setEnabled(false);
        chaseCam = new ChaseCamera(cam, cube, inputManager);
        chaseCam.setSmoothMotion(true);
        chaseCam.setDragToRotate(true);
        chaseCam.setDefaultDistance(50f);
        chaseCam.setMaxDistance(100f);
        viewPort.setBackgroundColor(ColorRGBA.Blue);
        DirectionalLight sun = new DirectionalLight();
        sun.setColor(ColorRGBA.Yellow);
        sun.setDirection(new Vector3f(-.5f,-.5f,-.5f).normalizeLocal());
        rootNode.addLight(sun);
        BasicShadowRenderer bsr = new BasicShadowRenderer(assetManager, 256);
        bsr.setDirection(new Vector3f(-.5f,-.5f,-.5f).normalizeLocal());
        viewPort.addProcessor(bsr);
        initThrowback();
    }

    private void initHero() {
        cube = new Geometry("hero", new PQTorus(2, 4, 6f, 8f, 10, 12));
        ExplodeControl exp = new ExplodeControl(this);
        cube.addControl(exp);
        cube.setMaterial(new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.j3md"));
        cube.move(0, 50, 0);
        cube.scale(0.1f);
        cube.rotate(new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat());
        RigidBodyControl cuberigid = new RigidBodyControl(1f);
        cube.addControl(cuberigid);
        getPhysSpace().getPhysicsSpace().add(cube);
        rootNode.attachChild(cube);
    }

    private void initThrowback() {
        fpp = new FilterPostProcessor(assetManager);
        viewPort.addProcessor(fpp);
        throwback = new ColorOverlayFilter(ColorRGBA.White);
        fpp.addFilter(throwback);
    }

    private void startThrowback() {
        speed = 0.25f;
        throwback.setColor(new ColorRGBA(0, 255, 255, 255));
    }

    private void stopThrowback() {
        speed = 1.0f;
        throwback.setColor(ColorRGBA.White);
    }

    public void onAnalog(String name, float value, float tpf) {
        if (cube != null) {
            float force = value * tpf * 10000;
            if (name.equals("DOWN")) {
                cube.getControl(RigidBodyControl.class).applyCentralForce(new Vector3f(force, 0, 0));
            } else if (name.equals("UP")) {
                cube.getControl(RigidBodyControl.class).applyCentralForce(new Vector3f(-force, 0, 0));
            } else if (name.equals("LEFT")) {
                cube.getControl(RigidBodyControl.class).applyCentralForce(new Vector3f(0, 0, force));
            } else if (name.equals("RIGHT")) {
                cube.getControl(RigidBodyControl.class).applyCentralForce(new Vector3f(0, 0, -force));
            } else if (name.equals("JUMP")) {
                cube.getControl(RigidBodyControl.class).applyCentralForce(new Vector3f(0, force, 0));
            } else if (name.equals("DIVE")) {
                cube.getControl(RigidBodyControl.class).applyCentralForce(new Vector3f(0, -10 *force, 0));
            } else if (name.equals("CLEAR")) {
                cube.getControl(RigidBodyControl.class).clearForces();
                cube.getControl(RigidBodyControl.class).applyCentralForce(cube.getControl(RigidBodyControl.class).getLinearVelocity().negate().mult(force));
            }
        }
    }

    private ScreenController scr;

    private void initInput() {
        inputManager.deleteMapping(INPUT_MAPPING_EXIT);

        inputManager.addMapping("LEFT", new KeyTrigger(KeyInput.KEY_LEFT), new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("UP", new KeyTrigger(KeyInput.KEY_UP), new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("DOWN", new KeyTrigger(KeyInput.KEY_DOWN), new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("RIGHT", new KeyTrigger(KeyInput.KEY_RIGHT), new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("JUMP", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("DIVE", new KeyTrigger(KeyInput.KEY_LSHIFT));
        inputManager.addMapping("ESCAPE", new KeyTrigger(KeyInput.KEY_ESCAPE));
        inputManager.addMapping("CLEAR", new KeyTrigger(KeyInput.KEY_Q));
        inputManager.addMapping("THBCK", new KeyTrigger(KeyInput.KEY_TAB));
        /*for (Joystick jo : inputManager.getJoysticks()) {
            jo.getXAxis().assignAxis("RIGHT", "LEFT");
            jo.getYAxis().assignAxis("DOWN", "UP");
            jo.getAxis("z").assignAxis("DIVE", "JUMP");
            jo.getButton("0").assignButton("THBCK");
            jo.getButton("1").assignButton("CLEAR");
            jo.getButton("7").assignButton("ESCAPE");
        }*/
        inputManager.addListener(this, new String[] { "LEFT", "RIGHT", "UP", "DOWN", "JUMP", "DIVE", "CLEAR" });
        inputManager.addListener(new ActionListener() {

            public void onAction(String name, boolean isPressed, float tpf) {
                if (name.equals("THBCK")) {
                    if (nif.getCurrentScreen().getScreenId().equals("gamovr")) {
                        stop();
                    }
                    if (throwback != null) {
                        if (isPressed) {
                            startThrowback();
                        } else {
                            stopThrowback();
                        }
                    }
                } else if (name.equals("ESCAPE")) {
                    if (isPressed) {
                        if (nif.getCurrentScreen().getScreenId().equals("hud")) {
                            nif.fromXml("pl/ijestfajnie/npc/LD26.xml", "gamovr", scr);
                        } else if (nif.getCurrentScreen().getScreenId().equals("start")) {
                            startGame();
                        } else {
                            nif.fromXml("pl/ijestfajnie/npc/LD26.xml", "hud", scr);
                        }
                    }
            }
            }
        }, new String[] {"THBCK", "ESCAPE"});
    }

    private void initSound() {
        audioRenderer.setEnvironment(Environment.Garage);
        AudioNode thing = new AudioNode(assetManager, "Thing.ogg", true);
        thing.setVolume(0.7f);
        thing.setPitch(new Random().nextFloat() + 0.5f);
        thing.setLooping(false);
        thing.setPositional(false);
        thing.setDirectional(false);
        thing.play();
    }
}